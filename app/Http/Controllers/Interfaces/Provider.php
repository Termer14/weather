<?php

namespace App\Http\Controllers\Interfaces;

interface Provider
{
    public function getByCityName();
}
