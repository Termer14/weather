<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Interfaces\Provider;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class OpenWeatherMap implements Provider
{
    private $city;
    private $api;

    public function __construct($city, $api) {
        $this->city = $city;
        $this->api = $api;
    }

    /**
    * Get city weather by name
    */

    public function getByCityName() {
        $data = [];
        try {
            $client = new Client();
            $request = $client->request('GET', 'api.openweathermap.org/data/2.5/weather', [
                'query' => ['appid' => $this->api, 'q' => $this->city, 'units' => 'metric']
            ]);
            $request_data = $request->getBody();
            $data['info'] = (string) $request_data;
            $data['success'] = true;
        } catch(RequestException $e) {
            $data['success'] = false;
        }
        
        return $data;
    }
}
