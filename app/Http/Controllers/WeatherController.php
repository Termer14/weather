<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OpenWeatherMap;

class WeatherController extends Controller
{
    /**
    * Controller for geting weather information
    */

    public function get(Request $request) {
        $provider = $request->input('provider');
        $city = $request->input('city');
        $api = $request->input('api');
        switch ($provider) {
            case 'open_weather_map':
                $weather = new OpenWeatherMap($city, $api);
                $data = $weather->getByCityName();
                break;
            default:
                $weather = new OpenWeatherMap($city, $api);
                $data = $weather->getByCityName();
        }
        return response()->json($data);
    }
}
