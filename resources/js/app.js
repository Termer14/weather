$(document).ready(function() {
    $('#submit').on('click', function() {
        get_weather();
    });
});

function get_weather() {
    var api = $("input[name='api']").val();
    var city = $("input[name='city']").val();
    var provider = 'open_weather_map';
    $.ajax({
        url: 'get_weather',
        type: 'post',
        data: {
            provider: provider,
            api: api, 
            city : city
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        success: function (data) {
            if(data.success) {
                addTab(data.info);
            } else {
                showError();
            }
        }
    });
}

function addTab(data) {
    var json = JSON.parse(data);
    var tab = '<li class="nav-item"><a class="nav-link active" id="tb'+json.id+'-tab" data-toggle="tab" href="#tb'+json.id+'" role="tab" aria-controls="tb'+json.id+'" aria-selected="true">'+json.name+'</a></li>';
    var tab_content = '<div class="tab-pane active" id="tb'+json.id+'" role="tabpanel" aria-labelledby="tb'+json.id+'-tab"><img src="http://openweathermap.org/img/w/'+json.weather[0].icon+'.png" alt="'+json.name+'"/><div class="weather-description">'+json.weather[0].description+'</div><div class="temp">'+Math.floor(json.main.temp)+'‎°C</div></div>';
    $('#weather-block').removeClass('hide');
    $('#weather-tab li a').removeClass('active');
    $('#weather-tab-content .tab-pane').removeClass('active');
    $('#weather-tab').append(tab);
    $('#weather-tab-content').append(tab_content);
}

function showError() {
    $('#errorModal').modal();
}